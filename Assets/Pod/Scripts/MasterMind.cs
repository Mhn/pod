﻿using UnityEngine;
using System.Collections.Generic;

public class MasterMind : MonoBehaviour
{
	[SerializeField] private float m_minimumHeight = 1.0f;

	public Vector3 Velocity    { get; set; }
	public Quaternion Rotation { get; set; }

	private List<LegControl> m_legs = new List<LegControl>();
	private Rigidbody m_body = null;
	private Vector3 m_globalVelocity;
	private float m_totalForce;
	//private Vector3 m_kineticForce = Vector3.zero;
	
	public void Awake()
	{
		m_body = GetComponent<Rigidbody>();

		//Direction = new Vector3(0.0f, 0.0f, 1.0f);
		//Velocity = 0.1f;
		//Rotation = Quaternion.AngleAxis(1f, new Vector3(0.0f, 1.0f, 0.0f));
	}

	public void AddLeg(LegControl leg)
	{
		m_legs.Add(leg);
		leg.Origin = transform;
	}
	
	public void RemoveLeg(LegControl leg)
	{
		m_legs.Remove(leg);
	}

	private void FixedUpdate()
	{
		RaycastHit hit;
		float distanceToGround = -1.0f;
		
		if (Physics.Raycast(transform.position, Vector3.down, out hit, 3.0f))
			distanceToGround = hit.distance;

		//m_minimumHeight = 0.5f * Mathf.Sin (Time.fixedTime) + 1.5f; 
		//Direction = new Vector3 (.5f * Mathf.Cos (Time.fixedTime * 1.5f), .5f * Mathf.Cos (Time.fixedTime * 2.0f), .5f * Mathf.Cos (Time.fixedTime));
		//Velocity = new Vector3 (0.0f, .5f * Mathf.Sin (Time.fixedTime), 0.0f);

		//Rotation = Quaternion.Euler (new Vector3 (Mathf.Cos (1.0f * Time.fixedTime), Mathf.Cos (1.0f * Time.fixedTime), Mathf.Cos (1.0f * Time.fixedTime)) * 5.0f);
		//Rotation = Quaternion.Euler (new Vector3 (0.0f, Mathf.Cos(1.0f * Time.fixedTime), 0.0f) * 30.0f);
		//Rotation = Quaternion.Euler (new Vector3 (10f * Mathf.Cos(Time.fixedTime), 20f * Mathf.Cos (Time.fixedTime * 10.0f), 10f * (Mathf.Cos (Time.fixedTime + Mathf.PI / 2.0f))));

		float lowestScore = .1f;
		int lowestIndex = -1;

		m_globalVelocity = transform.TransformVector(Velocity);
		m_totalForce = 0.0f;
		
		for (int i = 0; i < m_legs.Count; i++)
		{
			LegControl leg = m_legs[i];
			
			m_totalForce += Mathf.Max(leg.Force, 0.0f);

			//if (m_totalForce == 0.0f)
			//	continue;

			//float score = leg.Reach; //leg.Force / m_totalForce;
			//if (score < lowestScore && leg.State == LegControl.LegState.Down)
			//{
			//	lowestScore = score;
			//	lowestIndex = i;
			//}
		}

		float avgForce = m_legs.Count > 0 ? m_totalForce / m_legs.Count : 0.0f;

		for (int i = 0; i < m_legs.Count; i++)
		{
			LegControl leg = m_legs[i];

			//if (i == lowestIndex)
			//{
			//	//leg.SetState(LegControl.LegState.Up);
			//}

			float forceDiff = (avgForce - leg.Force) / m_totalForce;
			float weightDistForce = forceDiff;
			if (avgForce == 0.0f || m_totalForce == 0.0f)
				weightDistForce = 0.0f;
			else if (forceDiff > 0.0f && distanceToGround > m_minimumHeight)
				weightDistForce = 0.0f;
			else if (forceDiff < 0.0f && distanceToGround < m_minimumHeight)
				weightDistForce = 0.0f;
			else if (distanceToGround < 0)
				weightDistForce = 0.0f;
			
			//if (distanceToGround >= 0)
			//	weightDistForce += (m_minimumHeight - distanceToGround);
			
			float legDistanceToGround = -1.0f;
			if (Physics.Raycast(leg.transform.position, Vector3.down, out hit, 3.0f))
				legDistanceToGround = hit.distance;
			
			//if (distanceToGround >= 0)
			//	weightDistForce += distanceToGround - legDistanceToGround;

			leg.Velocity = leg.transform.InverseTransformVector(m_globalVelocity) + Vector3.up * weightDistForce;
			leg.Rotation = Rotation;
		}
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawLine(transform.position, transform.position + transform.TransformVector(Velocity));
		
		Gizmos.color = Color.red;
		Vector3 pos = transform.position + Vector3.up * m_totalForce * 0.001f;
		Gizmos.DrawLine(transform.position, pos);

		Gizmos.color = Color.white;

		/*
		for (int i = 0; i < m_legs.Count; i++)
		{
			LegControl leg = m_legs[i];
			if (m_totalForce != 0.0f)
				Gizmos.DrawWireSphere(leg.transform.position, leg.Force * leg.Reach / m_totalForce);
		}
		*/
	}
}
