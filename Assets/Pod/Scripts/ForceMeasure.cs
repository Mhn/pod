﻿using UnityEngine;
using System.Collections;

public class ForceMeasure : MonoBehaviour
{
	public Vector3 Force {
		get {
			if (m_time < Time.fixedTime)
				return CalculateForce();
			else
				return m_force;
		}
	}

	private ConfigurableJoint m_joint = null;
	private Transform m_body2 = null;
	private Vector3 m_force;
	private float m_time;
	
	private void Start()
	{
		m_joint = GetComponent<ConfigurableJoint>();
		m_body2 = m_joint.connectedBody.transform;
	}

	private Vector3 CalculateForce()
	{
		m_time = Time.fixedTime;
		m_force = m_joint.connectedAnchor - m_body2.InverseTransformPoint(transform.position);
		m_force.z *= m_joint.xDrive.positionSpring;
		return m_force;
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		
		Vector3 pos = transform.position + transform.TransformVector(-Force * 0.001f);
		Gizmos.DrawLine(transform.position, pos);
	}
}
