﻿using UnityEngine;
using System.Collections;

public class LegControl : MonoBehaviour
{
	public enum LegState { Down, Up }

	[SerializeField] private FixedJoint m_attachment = null;
	[SerializeField] private HingeJoint m_coxa = null;
	[SerializeField] private HingeJoint m_femur = null;
	[SerializeField] private HingeJoint m_tibia = null;
	[SerializeField] private HingeJoint m_tarsus = null;
	[SerializeField] private ForceMeasure m_sensor = null;
	[SerializeField] private Transform m_toe = null;
	
	[SerializeField] private float m_coxaLength = 0.5f;
	[SerializeField] private float m_femurLength = 1f;
	[SerializeField] private float m_tibiaLength = 1f;
	[SerializeField] private float m_tarsusLength = 1f;

	public Vector3 Velocity    { private get; set; }
	public Quaternion Rotation { private get; set; }
	public Transform Origin    { private get; set; }
	public LegState State      { get; private set; }
	public float Force { get { return m_sensor.Force.z; } }
	public float Reach { get { return m_reach; } }
	
	public Vector3 TargetPosition3D { get { return m_targetPos3d; } }
	public Vector2 TargetPosition2D { get { return m_targetPos2d; } }
	public float CoxaAngle { get { return m_targetCoxaAngle; } }
	public float FemurAngle { get { return m_targetFemurAngle; } }
	public float TibiaAngle { get { return m_targetTibiaAngle; } }
	public float TarsusAngle { get { return m_targetTarsusAngle; } }
	
	private Vector3 m_targetPos3d;
	private Vector2 m_targetPos2d;
	private Vector2 m_up2d;
	private float m_targetCoxaAngle = 0.0f;
	private float m_targetFemurAngle = 0.0f;
	private float m_targetTibiaAngle = 0.0f;
	private float m_targetTarsusAngle = 0.0f;
	private float m_reach = 0.0f;
	private float m_maxReach = 0.0f;
	
	private Vector3 m_targetPos3dOrigin;
	
	private void OnEnable()
	{
		m_maxReach = m_coxaLength + m_femurLength + m_tibiaLength + m_tarsusLength;
		State = LegState.Up;
		MasterMind m = transform.parent.parent.GetComponentInChildren<MasterMind>();
		Rigidbody r = m.GetComponent<Rigidbody>();
		m_attachment.connectedBody = r;
		m.AddLeg(this);
	}

	private void OnDisable()
	{
		MasterMind m = transform.parent.parent.GetComponentInChildren<MasterMind>();
		m.RemoveLeg(this);
	}
	
	private void Calculate3dPosition(ref Vector3 targetPos)
	{
		targetPos = new Vector3 (0.0f, -1.0f, 1.5f);
		//targetPos.y += 0.5f;
		//targetPos.z -= 0.5f;
		// Apply velocity and stuff
	}
	
	private void Calculate2dPosition(Vector3 targetPos, ref Vector2 pos2d)
	{
		pos2d.x = Mathf.Sqrt (targetPos.x * targetPos.x + targetPos.z * targetPos.z);
		pos2d.y = targetPos.y;
	}
	
//	private float GetCurrentReach(Vector3 targetPos, ref Vector2 pos2d)
//	{
//		pos2d.x = Mathf.Sqrt(targetPos.x * targetPos.x + targetPos.z * targetPos.z);
//		pos2d.y = targetPos.y;
//		pos2d.x -= m_coxaLength;
//
//		float targetReach = targetPos.magnitude;
//		float legLength = m_coxaLength + m_femurLength + m_tibiaLength;
//
//		return (1.0f - targetReach / legLength);
//	}
//	
//	private bool IsPositionReachable(Vector2 pos2d)
//	{
//		float targetReach = pos2d.magnitude;
//		if (targetReach > m_femurLength + m_tibiaLength)
//			return false;
//		else if (targetReach < Mathf.Abs (m_femurLength - m_tibiaLength))
//			return false;
//		else if (targetReach == 0.0f)
//			return false;
//		return true;
//	}
//	
//	private bool IsPositionReached(Vector3 targetPos)
//	{
//		float dist = (m_attachment.transform.InverseTransformPoint(m_tarsus.transform.position) - targetPos).magnitude;
//
//		return dist < 0.5f;
//	}

	private float GetTargetCoxaAngle(Vector3 targetPos)
	{
		float angle = Mathf.Atan2(targetPos.x, targetPos.z) * Mathf.Rad2Deg;
		if (angle > 90.0f)
			angle -= 180.0f;
		else if (angle < -90.0f)
			angle += 180.0f;
		return angle;
	}
	
	private float LawOfCosine(float c, float a, float b, float c2, float a2, float b2)
	{
		return Mathf.Acos( -(c2 - a2 - b2) / (2.0f * a * b));
	}

	private static float GetAngle(Vector2 v1, Vector2 v2)
	{
		var sign = Mathf.Sign(v1.x * v2.y - v1.y * v2.x);
		return Vector2.Angle(v1, v2) * sign;
	}

	public Vector2 Rotate(Vector2 v, float degrees)
	{
		return Quaternion.Euler(0, 0, degrees) * v;
	}
	
	private void GetTargetFemurAngle(Vector2 targetPos, ref float targetFemurAngle, ref float targetTibiaAngle, ref float targetTarsusAngle)
	{
		float targetReach = targetPos.magnitude;
		float tibia2 = m_tibiaLength * m_tibiaLength;
		float femur2 = m_femurLength * m_femurLength;
		float tarsus2 = m_tarsusLength * m_tarsusLength;
		float target2 = targetReach * targetReach;
		
		float femurInternalAngleRadians = 0.0f;
		float tibiaInternalAngleRadians = Mathf.PI;
		float tarsusInternalAngleRadians = 0.0f;
		
		if (targetReach > m_maxReach)
		{
			targetFemurAngle = Mathf.Atan2(targetPos.y, targetPos.x) * Mathf.Rad2Deg;
			targetTibiaAngle = 0.0f;
			targetTarsusAngle = 0.0f;
		}
		else
		{
			float tarsusXtargetMaxAngle = 180.0f;

			if (targetReach > m_maxReach - m_tarsusLength)
				tarsusXtargetMaxAngle = LawOfCosine(m_maxReach, m_tarsusLength, targetReach, m_maxReach*m_maxReach, tarsus2, target2);

			float upAngle = GetAngle(m_up2d, -targetPos);
			
			float tarsusXtargetAngle = Mathf.Clamp(upAngle, -tarsusXtargetMaxAngle, tarsusXtargetMaxAngle);
			
			targetPos -= Rotate(-targetPos.normalized, tarsusXtargetAngle) * m_tarsusLength;
			
			targetReach = targetPos.magnitude;
			target2 = targetReach * targetReach;
			
			if (targetReach < m_femurLength + m_tibiaLength)
			{
				femurInternalAngleRadians  = LawOfCosine(m_tibiaLength, m_femurLength, targetReach,   tibia2,  femur2,  target2);
				tibiaInternalAngleRadians  = LawOfCosine(targetReach,   m_tibiaLength, m_femurLength, target2, tibia2,  femur2);
				tarsusInternalAngleRadians = LawOfCosine(m_femurLength, targetReach,   m_tibiaLength, femur2,  target2, tibia2);
			}
			
			float femurAngleRadians = femurInternalAngleRadians + Mathf.Atan2 (targetPos.y, targetPos.x);
			float tibiaAngleRadians = Mathf.PI - tibiaInternalAngleRadians;
			//float tarsusAngleRadians = tarsusInternalAngleRadians + 180.0;
			float targetAngle = Mathf.Atan2(targetPos.y, targetPos.x) * Mathf.Rad2Deg;

			targetFemurAngle = femurAngleRadians * Mathf.Rad2Deg;
			targetTibiaAngle = tibiaAngleRadians * Mathf.Rad2Deg;
			targetTarsusAngle = tarsusXtargetAngle; // upAngle; //tarsusInternalAngleRadians * Mathf.Rad2Deg +  tarsusXtargetAngle; //180.0f * 3.0f - targetFemurAngle - (180.0f - targetTibiaAngle) - tarsusXtargetAngle - targetAngle;
		}
	}
	
	private void ControlJoint(HingeJoint joint, float targetAngle)
	{
		JointMotor motor = joint.motor;
		motor.targetVelocity = 10.0f * (targetAngle - joint.angle);
		joint.motor = motor;
	}

//	public void SetState(LegState state)
//	{
//		State = state;
//		m_targetPos3d += Vector3.up * Time.fixedDeltaTime;
//	}

	private void FixedUpdate()
	{
		m_targetPos3d = m_attachment.transform.InverseTransformPoint(m_toe.position);
		
		Calculate3dPosition(ref m_targetPos3d);
		Calculate2dPosition(m_targetPos3d, ref m_targetPos2d);
		m_targetPos2d.x -= m_coxaLength;

		Vector3 coxaUp = m_coxa.transform.InverseTransformDirection(Vector3.up);
		float distance = Vector3.Dot(Vector3.right, coxaUp);
		coxaUp -= (Vector3.right * distance);
		
		m_up2d.x = coxaUp.z;
		m_up2d.y = coxaUp.y;
		m_up2d.Normalize();

		//Calculate2dPosition(m_attachment.transform.InverseTransformDirection(Vector3.up), ref m_up2d);
		
		//if (State == LegState.Down && m_reach < 0.0f)
		//{
		//	State = LegState.Up;
		//	m_targetPos3d += Vector3.up * Time.fixedDeltaTime;
		//}



		//if (State == LegState.Down)
		//{
		//	m_targetPos3dOrigin = Origin.InverseTransformPoint(transform.TransformPoint(m_targetPos3d));
		//	m_targetPos3dOrigin = Quaternion.Slerp(Quaternion.identity, Rotation, Time.fixedDeltaTime) * m_targetPos3dOrigin;
		//	m_targetPos3d = transform.InverseTransformPoint (Origin.TransformPoint (m_targetPos3dOrigin));
		//
		//	m_targetPos3d -= Velocity * Time.fixedDeltaTime;
		//}
		//else
		//	m_targetPos3d += Velocity.normalized * Time.fixedDeltaTime * 5.0f;

		//float maxReach = m_coxaLength + m_femurLength + m_tibiaLength + m_tarsusLength;


		m_targetCoxaAngle = GetTargetCoxaAngle(m_targetPos3d);
		
//		m_reach = GetCurrentReach(m_targetPos3d, ref m_targetPos2d) * (90.0f - Mathf.Abs(m_targetCoxaAngle)) / 90.0f;

		GetTargetFemurAngle(m_targetPos2d, ref m_targetFemurAngle, ref m_targetTibiaAngle, ref m_targetTarsusAngle);

		ControlJoint(m_coxa, m_targetCoxaAngle);
		ControlJoint(m_femur, m_targetFemurAngle);
		ControlJoint(m_tibia, m_targetTibiaAngle);
		ControlJoint(m_tarsus, m_targetTarsusAngle);

		//else if (State == LegState.Down)
		//{
		//	State = LegState.Up;
		//	m_targetPos3d += Vector3.up * Time.fixedDeltaTime;
		//}

		//if (State == LegState.Up && IsPositionReached(m_idealPosition))
		//{
		//	State = LegState.Down;
		//	m_targetPos3d = m_idealPosition;
		//}
	}
	
	//private void OnDrawGizmosSelected()
	//{
	//}
	
	private void OnDrawGizmos()
	{
		//Gizmos.color = Color.red;

		//Vector3 idealPos = m_coxa.transform.position + m_attachment.transform.TransformVector(m_idealPosition);
		//Gizmos.DrawLine(m_coxa.transform.position, m_coxa.transform.position + m_attachment.transform.TransformVector(m_idealPosition));

		Gizmos.color = State == LegState.Down ? Color.yellow : Color.blue;
		Vector3 targetPos = m_attachment.transform.TransformPoint(m_targetPos3d);
		Gizmos.DrawLine(m_toe.position, targetPos);

		//Oryx.Log.Debug ("leg: ", transform.parent.name, " up: ", m_up2d);
		
		//Gizmos.color = Color.green;
		//Gizmos.DrawLine(m_toe.position, targetPos + m_attachment.transform.TransformVector(-Velocity));
		
		//Gizmos.color = Color.white;
		//Gizmos.DrawWireSphere(m_attachment.transform.position, m_reach * Force);

	}
}
