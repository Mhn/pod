﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LegControl))]
public class LegControlInspector : Editor
{
	public override void OnInspectorGUI()
	{
		LegControl leg = (LegControl)target;
		
		DrawDefaultInspector();
		EditorGUILayout.Space();
		
		bool enabled = GUI.enabled;
		GUI.enabled = EditorApplication.isPlaying;
		
		EditorGUILayout.FloatField("coxaTargetAngle", leg.CoxaAngle);
		EditorGUILayout.FloatField("femurTargetAngle", leg.FemurAngle);
		EditorGUILayout.FloatField("tibiaTargetAngle", leg.TibiaAngle);
		EditorGUILayout.FloatField("tarsusTargetAngle", leg.TarsusAngle);
		EditorGUILayout.Vector2Field ("TargetPosition2D", leg.TargetPosition2D);
		EditorGUILayout.Vector3Field ("TargetPosition3D", leg.TargetPosition3D);

		
		
		GUI.enabled = enabled;
	}
}